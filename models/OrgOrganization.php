<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "org_organization".
 *
 * @property int $id_organization
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $website
 * @property resource $logo
 * @property int $created_at
 * @property int $updated_at
 * @property int $user_id
 */
class OrgOrganization extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'org_organization';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'phone', 'email', 'website', 'logo', 'created_at', 'updated_at'], 'required'],
            [['created_at', 'updated_at', 'user_id'], 'integer'],
            [['logo'], 'string'],
            [['name'], 'string', 'max' => 128],
            [['phone'], 'string', 'max' => 13],
            [['email', 'website'], 'string', 'max' => 64],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_organization' => 'Id Organization',
            'name' => 'Name',
            'phone' => 'Phone',
            'email' => 'Email',
            'website' => 'Website',
            'logo' => 'Logo',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'user_id' => 'Account Manager',
        ];
    }
}
