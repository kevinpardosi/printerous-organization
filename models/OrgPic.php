<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "org_pic".
 *
 * @property int $id_pic
 * @property int $organization_id
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property resource $avatar
 * @property int $created_at
 * @property int $updated_at
 */
class OrgPic extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'org_pic';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['organization_id', 'name', 'email', 'phone', 'avatar', 'created_at', 'updated_at'], 'required'],
            [['organization_id', 'created_at', 'updated_at'], 'integer'],
            [['avatar'], 'string'],
            [['name'], 'string', 'max' => 128],
            [['phone'], 'string', 'max' => 13],
            [['email'], 'string', 'max' => 64],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_pic' => 'Id Pic',
            'organization_id' => 'Organization ID',
            'name' => 'Name',
            'email' => 'Email',
            'phone' => 'Phone',
            'avatar' => 'Avatar',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
