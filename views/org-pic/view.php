<?php

use yii\helpers\Html;
use yii\widgets\DetailView;


/* @var $this yii\web\View */
/* @var $model app\models\OrgPic */

$this->title = $model->name;
$orgModel = app\models\OrgOrganization::find()->where(['id_organization' => $model->organization_id])->one();
$user_id = $orgModel->user_id;

$orgModel = app\models\OrgOrganization::find()->where(['id_organization' => $model->organization_id])->one();
$nama = $orgModel->name;
$this->params['breadcrumbs'][] = ['label' => $nama, 'url' => ['/org-organization/view', 'id' => $model->organization_id]];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id_pic]];
\yii\web\YiiAsset::register($this);
?>
<div class="org-pic-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php 
        $server = "http://" . $_SERVER['HTTP_HOST'];
        $projectname = Yii::getAlias('@web');
        $pathLogo  = Yii::getAlias('/trash/avatar/');
        $logoName = $model->avatar;
    ?>
    <p>
        <img src="<?php echo $server . $projectname . $pathLogo . $logoName ?>" width="170" height="200" alt="PIC-Avatar" align="center" class="company-logo">
    </p>
    
    <?php if($user_id == Yii::$app->user->id){ ?>
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id_pic], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id_pic], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?php } ?>

    <h3><?php echo 'PIC Profile' ?></h3>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [            
            [
                'attribute' => 'organization_id',
                'label' => 'Organization',
                'value' => function ($model) {
                    $orgModel = app\models\OrgOrganization::find()->where(['id_organization' => $model->organization_id])->one();
                    $nama = $orgModel->name;
                    return ucfirst($nama);
                },
            ],
            'name',
            'email:email',
            'phone',
            'avatar',
        ],
    ]) ?>

</div>
