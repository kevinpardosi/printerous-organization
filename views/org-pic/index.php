<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\search\OrgPicSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'PICs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="org-pic-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create New', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php 
    echo GridView::widget([
        'filterModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'options' => ['style' => 'font-size:12px;'],
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
            'options' => [
                'id' => '-pjax',
                'enableReplaceState' => false,
                'enablePushState' => false,
            ],
        ],
        'striped' => true,
        'hover' => true,
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'tableOptions' => ['class' => 'table table-hover'],
        'resizableColumns' => true,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn',
                'header' => 'No',
                'options' => [
                    'width' => '10px',
                ],
            ],

            'organization_id',
            'name',
            'email',
            'phone',
            ['class' => 'yii\grid\ActionColumn'],
        ],   
        'toolbar' => [
            ['content' =>                
                  Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => Yii::t('app', 'Reset Grid')])
            ],
            '{toggleData}',
        ],
        'panel' => [
            'type' => GridView::TYPE_SUCCESS,
            'after' => '<em>* Atur lebar kolom dengan cara menarik garis tepi kolom.</em>',
            'heading' => '<i class="glyphicon glyphicon-align-left"></i>&nbsp;&nbsp;<b>PIC List</b>',
        ],
        'persistResize' => false,
        'toggleDataOptions' => ['minCount' => 10]
    ]); 
    ?>
</div>