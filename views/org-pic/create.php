<?php

use yii\helpers\Html;
use app\models\OrgOrganization;

/* @var $this yii\web\View */
/* @var $model app\models\OrgPic */

$this->title = 'Create PIC';
$org_id = Yii::$app->getRequest()->getQueryParam('id');
$orgModel = OrgOrganization::find()->where(['id_organization' => $org_id])->one();
$org_name = $orgModel->name;

$this->params['breadcrumbs'][] = ['label' => $org_name, 'url' => ['/org-organization/view', 'id' => $org_id]];
?>
<div class="org-pic-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,        
    ]) ?>

</div>
