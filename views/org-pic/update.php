<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\OrgPic */

$orgModel = app\models\OrgOrganization::find()->where(['id_organization' => $model->organization_id])->one();
$nama = $orgModel->name;
$this->title = 'Update PIC: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => $nama, 'url' => ['/org-organization/view', 'id' => $model->organization_id]];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id_pic]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="org-pic-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
