<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\OrgPic */
/* @var $form yii\widgets\ActiveForm */

$org_id = Yii::$app->getRequest()->getQueryParam('id');
?>

<div class="org-pic-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput() ?>
    
    <?=
    $form->field($model, 'avatar')->widget(FileInput::classname(), [  
        'options' => [
            'multiple' => false,
            'accept' => 'img/*',
            'class' => 'optionvalue-img',
            'placeholder' => 'maximum size is 2 MB',            
        ],
        'pluginOptions' => [
            'allowedFileExtensions' => ['jpg', 'png', 'jpeg'],
            'maxFileSize' => 2048, 
            'layoutTemplates' => ['footer' => 'Maximum size is 2 MB'],
            'browseLabel' => 'Browse (2MB)',
            'showPreview' => true,
            'showCaption' => true,
            'showRemove' => true,
            'showUpload' => false,
        ]
    ]);
    ?>

    <div class="form-group">
        <?php echo '<div class="text-right" style="margin-right: 18px">' . Html::resetButton('<i class="glyphicon glyphicon-refresh"></i> Reset', ['class' => 'btn btn-default']) . ' ' . Html::submitButton('<i class="glyphicon glyphicon-save"></i> Submit', ['class' => 'btn btn-primary', 'id' => 'btn-create-bp', 'value' => $org_id]) . '</div>';
        ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
