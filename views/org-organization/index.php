<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $searchModel app\search\OrgOrganizationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Organizations';
?>
<div class="org-organization-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('<i class="glyphicon glyphicon-plus"></i> Organization', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php 
    echo GridView::widget([
        'filterModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'options' => ['style' => 'font-size:12px;'],
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
            'options' => [
                'id' => '-pjax',
                'enableReplaceState' => false,
                'enablePushState' => false,
            ],
        ],
        'striped' => true,
        'hover' => true,
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'tableOptions' => ['class' => 'table table-hover'],
        'resizableColumns' => true,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn',
                'header' => 'No',
                'options' => [
                    'width' => '10px',
                ],
            ],
            [
                'attribute' => 'name',
                'format' => 'raw',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'width' => '748px'
            ],
            [
                'attribute' => 'user_id',
                'format' => 'raw',
                'value' => function ($data) {
                    $modelUser = dektrium\user\models\User::find()->where(['id' => $data->user_id])->one();
                    $nama = $modelUser->username;
                    return ucfirst($nama);
                },
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'width' => '300px',
                'mergeHeader' => true
            ],
            ['class' => 'yii\grid\ActionColumn',
                'options' => [
                    'width' => 58
                ],
                'visibleButtons' => [
                    'update' => function ($model, $key, $index) {
                        return $model->user_id === \Yii::$app->user->id ? true : false;
                    },
                    'delete' => function ($model, $key, $index) {
                        return $model->user_id === \Yii::$app->user->id ? true : false;
                    }
                ]
            ],
        ],    
        'toolbar' => [
            ['content' =>                
                  Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => Yii::t('app', 'Reset Grid')])
            ],
            '{toggleData}',
        ],
        'panel' => [
            'type' => GridView::TYPE_SUCCESS,
            'after' => '<em>* Atur lebar kolom dengan cara menarik garis tepi kolom.</em>',
            'heading' => '<i class="glyphicon glyphicon-align-left"></i>&nbsp;&nbsp;<b>Organization List</b>',
        ],
        'persistResize' => false,
        'toggleDataOptions' => ['minCount' => 10]
    ]); 
    ?>
</div>
