<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\OrgOrganization */

$this->title = 'Create Organization';
$this->params['breadcrumbs'][] = ['label' => 'Organization List', 'url' => ['index']];
?>
<div class="org-organization-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
