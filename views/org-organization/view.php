<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\data\ActiveDataProvider;

/* @var $this yii\web\View */
/* @var $model app\models\OrgOrganization */

$this->title = $model->name;
$listTitle = 'PIC List';
$this->params['breadcrumbs'][] = ['label' => 'Organizations', 'url' => ['index']];
\yii\web\YiiAsset::register($this);
?>
<div class="org-organization-view">

    <h1><?= Html::encode($this->title) ?></h1>
      
    <?php 
        $server = "http://" . $_SERVER['HTTP_HOST'];
        $projectname = Yii::getAlias('@web');
        $pathLogo  = Yii::getAlias('/trash/logo/');
        $logoName = $model->logo;
    ?>
    <p>
        <img src="<?php echo $server . $projectname . $pathLogo . $logoName ?>" width="170" height="200" alt="Company-Logo" align="center" class="company-logo">
    </p>
         
    <?php if($model->user_id == Yii::$app->user->id){ ?>
        <p>
        <?= Html::a('Update', ['update', 'id' => $model->id_organization], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id_organization], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?php } ?>    

    <h3><?php echo 'Company Profile' ?></h3>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'phone',
            'email:email',
            'website'
        ],
    ]) ?>
    
    
    <?php if($model->user_id == Yii::$app->user->id){ ?>
    <h3><?php echo $listTitle?></h3>
    
    <?php if($model->user_id == Yii::$app->user->id){ ?>
    <p>
        <?= Html::a('<i class="glyphicon glyphicon-plus"></i> <b>PIC</b>', ['/org-pic/create', 'id' => $model->id_organization], ['class' => 'btn btn-add-pic']) ?>
    </p>
    <?php } ?>    
    
    <?php    
    
    $searchModel = new app\search\OrgPicSearch();
    $dataProvider = new ActiveDataProvider([
        'query' => app\models\OrgPic::find()->where(['organization_id' => $model->id_organization]),
        'pagination' => [
            'pageSize' => 5,
        ],
    ]);
    
    echo GridView::widget([
        'filterModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'options' => ['style' => 'font-size:12px;'],
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
            'options' => [
                'id' => '-pjax',
                'enableReplaceState' => false,
                'enablePushState' => false,
            ],
        ],
        'striped' => true,
        'hover' => true,
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'tableOptions' => ['class' => 'table table-hover'],
        'resizableColumns' => true,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn',
                'header' => 'No',
                'options' => [
                    'width' => '10px',
                ],
            ],
            [
                'attribute' => 'name',
                'format' => 'raw',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'width' => '548px'
            ],
            [
                'attribute' => 'phone',
                'format' => 'raw',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'width' => '200px',
                'mergeHeader' => true
            ],
            [
                'attribute' => 'email',
                'format' => 'raw',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'width' => '300px',
                'mergeHeader' => true
            ],
            [
                'header' => 'Action',
                'class' => 'yii\grid\ActionColumn',
                'headerOptions' => ['width' => '65', 'align' => 'center'],
                'visibleButtons' => [
                    'view' => function ($model, $key, $index) {
                        $org_id = Yii::$app->getRequest()->getQueryParam('id');
                        $orgModel = app\models\OrgOrganization::find()->where(['id_organization' => $org_id])->one();
                        $user_id = $orgModel->user_id;
                        return $user_id === \Yii::$app->user->id ? true : false;
                    },
                    'update' => function ($model, $key, $index) {
                        $org_id = Yii::$app->getRequest()->getQueryParam('id');
                        $orgModel = app\models\OrgOrganization::find()->where(['id_organization' => $org_id])->one();
                        $user_id = $orgModel->user_id;
                        return $user_id === \Yii::$app->user->id ? true : false;
                    },
                    'delete' => function ($model, $key, $index) {
                        $org_id = Yii::$app->getRequest()->getQueryParam('id');
                        $orgModel = app\models\OrgOrganization::find()->where(['id_organization' => $org_id])->one();
                        $user_id = $orgModel->user_id;
                        return $user_id === \Yii::$app->user->id ? true : false;
                    }
                ],
                'template' => '{view}&nbsp;&nbsp;{update}&nbsp;&nbsp;{delete}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<i class="glyphicon glyphicon-eye-open"></i>', $url, [
                                    'title' => Yii::t('app', 'view'),
                                    'data-toggle' => "modal",
                                    'data-target' => "#myModal",
                                    'data-method' => 'post',
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<i class="glyphicon glyphicon-trash"></i>', $url, [
                                    'title' => Yii::t('app', 'delete'),
                                    'data-toggle' => "modal",
                                    'data-target' => "#myModal",
                                    'data-method' => 'post',
                        ]);
                    },
                    'update' => function ($url, $model) {
                        return Html::a('<i class="glyphicon glyphicon-edit"></i>', $url, [
                                    'title' => Yii::t('app', 'update'),
                                    'data-toggle' => "modal",
                                    'data-target' => "#myModal",
                                    'data-method' => 'post',
                        ]);
                    },
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'view') {
                        $url = Url::toRoute(['/org-pic/view', 'id' => $model->organization_id], ['data-method' => 'post',]);
                        return $url;
                    } else if ($action === 'delete') {
                        $url = Url::toRoute(['/org-pic/delete', 'id' => $model->organization_id], ['data-method' => 'post',]);
                        return $url;
                    } else if ($action === 'update') {
                        $url = Url::toRoute(['/org-pic/update', 'id' => $model->organization_id], ['data-method' => 'post',]);
                        return $url;
                    }
                }
            ],
        ],  
        'persistResize' => false,
        'toggleDataOptions' => ['minCount' => 10]
    ]); 
    ?>   
    
    <?php } ?>
</div>
