<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\OrgOrganization */

$this->title = 'Update Organization: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Organizations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id_organization]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="org-organization-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
