<?php

namespace app\controllers;

use Yii;
use app\models\OrgOrganization;
use app\search\OrgOrganizationSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * OrgOrganizationController implements the CRUD actions for OrgOrganization model.
 */
class OrgOrganizationController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all OrgOrganization models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrgOrganizationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single OrgOrganization model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new OrgOrganization model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new OrgOrganization();
        $userId = Yii::$app->user->id;

        if ($model->load(Yii::$app->request->post())) {
            $dirTrash = Yii::getAlias('@webroot/trash');
            $dirSubTrash = Yii::getAlias('@webroot/trash/logo');

            if (!is_dir($dirTrash)) {
                mkdir(Yii::getAlias('@webroot/trash'));
                if (!is_dir($dirSubTrash)) {
                    mkdir(Yii::getAlias('@webroot/trash/logo'));
                }
            } else {
                if (!is_dir($dirSubTrash)) {
                    mkdir(Yii::getAlias('@webroot/trash/logo'));
                }
            }
            $model->logo = UploadedFile::getInstance($model, 'logo');
            if($model->logo){
                    $imgIdLogo = mt_rand(100000, 999999);
                    $nameLogo = 'Logo' . $imgIdLogo . '-' . $model->logo->baseName . '.' . $model->logo->extension;
                    $pathLogo = Yii::getAlias('@webroot/trash/logo/') . $nameLogo;
                    $model->logo->saveAs($pathLogo);
                    $model->logo = $nameLogo;
            }
            $model->name = $model->name;
            $model->phone = "'".$model->phone."'";
            $model->email = $model->email;
            $model->website = $model->website;
            $model->user_id = $userId;
            $model->save(false);
            return $this->redirect(['view', 'id' => $model->id_organization]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing OrgOrganization model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        
        $ac_id = $model->user_id;
        if($ac_id == Yii::$app->user->id){    
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id_organization]);
            }

            return $this->render('update', [
                'model' => $model,
            ]);
        } else {
            throw new \yii\web\ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }
    }

    /**
     * Deletes an existing OrgOrganization model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {        
        $organizationModel = OrgOrganization::find()->where(['id_organization' => $id])->one();
        $ac_id = $organizationModel->user_id;
        if($ac_id == Yii::$app->user->id){    
            $this->findModel($id)->delete();
            return $this->redirect(['index']);
        } else {
            throw new \yii\web\ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }
    }

    /**
     * Finds the OrgOrganization model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OrgOrganization the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = OrgOrganization::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
