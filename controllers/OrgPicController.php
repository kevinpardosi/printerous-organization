<?php

namespace app\controllers;

use Yii;
use app\models\OrgPic;
use app\search\OrgPicSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * OrgPicController implements the CRUD actions for OrgPic model.
 */
class OrgPicController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all OrgPic models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrgPicSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single OrgPic model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $organizationModel = \app\models\OrgOrganization::find()->where(['id_organization' => $model->organization_id])->one();
        $ac_id = $organizationModel->user_id;
        if($ac_id == Yii::$app->user->id){
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        } else {
            throw new \yii\web\ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }   
    }

    /**
     * Creates a new OrgPic model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $model = new OrgPic();
       
        $organizationModel = \app\models\OrgOrganization::find()->where(['id_organization' => $id])->one();
        $ac_id = $organizationModel->user_id;
        if($ac_id == Yii::$app->user->id){    
            if ($model->load(Yii::$app->request->post())) {
                $dirSubTrash = Yii::getAlias('@webroot/trash/avatar');
                if (!is_dir($dirSubTrash)) {
                    mkdir(Yii::getAlias('@webroot/trash/avatar'));
                }
                $model->avatar = UploadedFile::getInstance($model, 'avatar');
                if($model->avatar){
                        $imgIdAvatar = mt_rand(100000, 999999);
                        $nameAvatar = 'Avatar' . $imgIdAvatar . '-' . $model->avatar->baseName . '.' . $model->avatar->extension;
                        $pathAvatar = Yii::getAlias('@webroot/trash/avatar/') . $nameAvatar;
                        $model->avatar->saveAs($pathAvatar);
                        $model->avatar = $nameAvatar;
                }
                $model->name = $model->name;
                $model->phone = $model->phone;
                $model->email = $model->email;
                $model->organization_id = $id;            
                $model->save(false);

                return $this->redirect(['view', 'id' => $model->id_pic]);
            }

            return $this->render('create', [
                'model' => $model,
            ]);
        } else {
            throw new \yii\web\ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }   
    }

    /**
     * Updates an existing OrgPic model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $organizationModel = \app\models\OrgOrganization::find()->where(['id_organization' => $model->organization_id])->one();
        $ac_id = $organizationModel->user_id;
        if($ac_id == Yii::$app->user->id){
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id_pic]);
            }
            
            return $this->render('update', [
                'model' => $model,
            ]);
        } else {
            throw new \yii\web\ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }            
    }

    /**
     * Deletes an existing OrgPic model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {      
        $organizationModel = \app\models\OrgOrganization::find()->where(['id_organization' => $id])->one();
        $ac_id = $organizationModel->user_id;
        if($ac_id == Yii::$app->user->id){    
            $this->findModel($id)->delete();
            return $this->redirect(['index']);
        } else {
            throw new \yii\web\ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }
    }

    /**
     * Finds the OrgPic model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OrgPic the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = OrgPic::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
