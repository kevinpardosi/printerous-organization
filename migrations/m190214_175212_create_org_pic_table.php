<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Handles the creation of table `{{%org_pic}}`.
 */
class m190214_175212_create_org_pic_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
          if ($this->db->driverName === 'mysql') {
              $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
          }
 
          $this->createTable('{{%org_pic}}', [
              'id_pic' => Schema::TYPE_PK,              
              'name' => Schema::TYPE_CHAR . '(128) NOT NULL',
              'email' => Schema::TYPE_CHAR . '(64) NOT NULL',
              'phone' => Schema::TYPE_INTEGER . '(13) NOT NULL',              
              'organization_id' => Schema::TYPE_INTEGER . '(11) NOT NULL',
              'avatar' => Schema::TYPE_BINARY . ' NOT NULL',
              'created_at' => Schema::TYPE_INTEGER . '(11) NOT NULL',
              'updated_at' => Schema::TYPE_INTEGER . '(11) NOT NULL'
          ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%org_pic}}');
    }
}
